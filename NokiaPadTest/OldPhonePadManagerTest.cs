using NokiaPad.Manager;

namespace NokiaPadTest;

public class OldPhonePadManagerTest
{
    [Theory]
    [InlineData("33#","E")]
    [InlineData("227*#", "B")]
    [InlineData("4433555 555666#", "HELLO")]
    [InlineData("8 88777444666*664#", "TURING")]
    public void PadConverter_WhenGotExpectedKeySequence_ShouldSolve(string sequence,string expected)
    {
        var manager = new OldPhonePadManager();

        var result = manager.PadConverter(sequence);

        Assert.Equal(expected, result);
    }
}