﻿namespace NokiaPad.Entities;

public class PressFrequeneAlphabet
{
    public int Pad { get; set; }

    public int Frequene { get; set; }

    public char Alphabet { get; set; }
}
