﻿using NokiaPad.Repositories;
using NokiaPad.Helpers;

namespace NokiaPad.Manager;

public class OldPhonePadManager
{
    private static readonly Alphabet _db = new();

    public string PadConverter(string input)
    {
        if (string.IsNullOrWhiteSpace(input) || !input.EndsWith('#'))
        {
            throw new ArgumentException("Bad Input");
        }

        var pressSequences = input.ToCharArray();

        var groupedSequences = SplitSequence(pressSequences);

        var solvedcharacterList = SolveGroupedSequence(groupedSequences);

        return solvedcharacterList.ToPhonePadString();
    }

    private static ICollection<char> SolveGroupedSequence(ICollection<string> groupedSequences)
    {
        var result = new Stack<char>();

        foreach (var sequence in groupedSequences)
        {
            if (string.IsNullOrWhiteSpace(sequence))
            {
                continue;
            }
            else if (sequence.Equals("#"))
            {
                break;
            }
            else if (sequence.Equals("*"))
            {
                result.TryPop(out char x);
            }
            else
            {
                int padNumber = int.Parse(sequence.First().ToString());
                int frequency = sequence.Length;
                var possibleAlphabet = _db.GetPadAlphabets(padNumber);
                int alphabetOnButton = possibleAlphabet.Count;
                char solvedChar = possibleAlphabet
                    .First(x => x.Frequene == (frequency - 1) % alphabetOnButton + 1)
                    .Alphabet;

                result.Push(solvedChar);
            }
        }

        return result.Reverse().ToList();
    }

    private static ICollection<string> SplitSequence(ICollection<char> pressSequence)
    {
        var result = new Stack<string>();

        for (int ii = 0; ii < pressSequence.Count; ++ii)
        {
            char currentChar = pressSequence.ElementAt(ii);

            if (ii == 0)
            {
                result.Push(currentChar.ToString());
            }

            if (ii > 0)
            {
                var top = result.Peek();

                if (currentChar.IsSpecialPad())
                {
                    result.Push(currentChar.ToString());
                }
                else if (top.Contains( currentChar))
                {
                    var prev = result.Pop();
                    result.Push(prev+currentChar);
                }
                else
                {
                    result.Push(currentChar.ToString());
                }
            } 
        }

        return result.Reverse().ToList();
    }
}
