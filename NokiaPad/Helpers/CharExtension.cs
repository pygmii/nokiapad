﻿namespace NokiaPad.Helpers;

public static class CharExtension
{
    public static string ToPhonePadString(this ICollection<char> message)
    {
        return new string(message.ToArray());
    }

    public static bool IsSpecialPad(this char pad)
    {
        char[] specialChar = new char[] { '*', '#', '0' };
        return specialChar.Contains(pad);
    }
}
