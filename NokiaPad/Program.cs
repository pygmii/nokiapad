﻿using NokiaPad.Manager;

namespace NokiaPad;

internal class Program
{
    static void Main(string[] args)
    {
        OldPhonePadManager manager = new();

        Console.WriteLine(manager.PadConverter("4433555 555666#"));
        Console.WriteLine(manager.PadConverter("8 88777444666*664#"));
        Console.WriteLine(manager.PadConverter("** 2 22 222#"));
        Console.WriteLine(manager.PadConverter("2022022202222#"));
        Console.WriteLine(manager.PadConverter("7 77 777 7777 77777#"));
    }
}