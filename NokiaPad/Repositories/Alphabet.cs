﻿using NokiaPad.Entities;

namespace NokiaPad.Repositories;

public class Alphabet
{
    private static ICollection<PressFrequeneAlphabet> Context  => CreateContext();

    public ICollection<PressFrequeneAlphabet> GetPadAlphabets(int pad)
    { 
        return Context.Where(x => x.Pad == pad).ToList();
    }

    private static ICollection<PressFrequeneAlphabet> CreateContext()
    {
        return new HashSet<PressFrequeneAlphabet>
        {
            new PressFrequeneAlphabet{ Pad = 1,Frequene=1, Alphabet='&' },
            new PressFrequeneAlphabet{ Pad = 1,Frequene=2, Alphabet='\'' },
            new PressFrequeneAlphabet{ Pad = 1,Frequene=3, Alphabet='(' },
            new PressFrequeneAlphabet{ Pad = 2,Frequene=1, Alphabet='A' },
            new PressFrequeneAlphabet{ Pad = 2,Frequene=2, Alphabet='B' },
            new PressFrequeneAlphabet{ Pad = 2,Frequene=3, Alphabet='C' },
            new PressFrequeneAlphabet{ Pad = 3,Frequene=1, Alphabet='D' },
            new PressFrequeneAlphabet{ Pad = 3,Frequene=2, Alphabet='E' },
            new PressFrequeneAlphabet{ Pad = 3,Frequene=3, Alphabet='F' },
            new PressFrequeneAlphabet{ Pad = 4,Frequene=1, Alphabet='G' },
            new PressFrequeneAlphabet{ Pad = 4,Frequene=2, Alphabet='H' },
            new PressFrequeneAlphabet{ Pad = 4,Frequene=3, Alphabet='I' },
            new PressFrequeneAlphabet{ Pad = 5,Frequene=1, Alphabet='J' },
            new PressFrequeneAlphabet{ Pad = 5,Frequene=2, Alphabet='K' },
            new PressFrequeneAlphabet{ Pad = 5,Frequene=3, Alphabet='L' },
            new PressFrequeneAlphabet{ Pad = 6,Frequene=1, Alphabet='M' },
            new PressFrequeneAlphabet{ Pad = 6,Frequene=2, Alphabet='N' },
            new PressFrequeneAlphabet{ Pad = 6,Frequene=3, Alphabet='O' },
            new PressFrequeneAlphabet{ Pad = 7,Frequene=1, Alphabet='P' },
            new PressFrequeneAlphabet{ Pad = 7,Frequene=2, Alphabet='Q' },
            new PressFrequeneAlphabet{ Pad = 7,Frequene=3, Alphabet='R' },
            new PressFrequeneAlphabet{ Pad = 7,Frequene=4, Alphabet='S' },
            new PressFrequeneAlphabet{ Pad = 8,Frequene=1, Alphabet='T' },
            new PressFrequeneAlphabet{ Pad = 8,Frequene=2, Alphabet='U' },
            new PressFrequeneAlphabet{ Pad = 8,Frequene=3, Alphabet='V' },
            new PressFrequeneAlphabet{ Pad = 9,Frequene=1, Alphabet='W' },
            new PressFrequeneAlphabet{ Pad = 9,Frequene=2, Alphabet='X' },
            new PressFrequeneAlphabet{ Pad = 9,Frequene=3, Alphabet='Y' },
            new PressFrequeneAlphabet{ Pad = 9,Frequene=4, Alphabet='Z' },
            new PressFrequeneAlphabet{ Pad = 0,Frequene=1, Alphabet=' ' }
        };
    }
}
